.. title: Doping Penalties
.. slug: doping-penalties
.. date: 2016-09-25 19:12:56 UTC+02:00
.. tags: doping, punishment, suspensions, collective penalties
.. category: rules
.. link: 
.. author: Daniel Roßbach, Jan Gertken
.. description: 
.. type: text

*Let's assume one, or maybe a few, players in a team use doping completely independent and with no involvement from the club. Should teams be punished for individuals doping?*

.. TEASER_END

For Collective Punishment
=========================

How can strictly individual doping in a team sport like football be judged and sanctioned fairly and effectively? We will see that there is no alternative to punishing teams as well as suspending players.

To show why, let's first give some argument for punishment aimed at teams being apt. Football is a team sport with no discernible individual contributions. Every piece of play - any counterattack, pressing wave, or set piece defence - involves any number of players both on and off the ball. While obviously some players' impact is larger (think Messi for Argentina) and others' is smaller, (think Mario Götze against France) in any case it is impossible to tease out anyone's share in the result in any measurable way. 

That means that we can not simply subtract a chemically enhanced performance, even quite apart from the confidence we may or may not have in everyone else's cleanness. So, we are left with two choices: only suspend the individual player caught cheating and perhaps fine the club, but letting its performances stand. Or treat the team performance as contaminated, annul it and award the game to the opposition. Punishing the whole team for one players failings is no more unfair than awarding the team points because they won a game as one of them played well while the rest was sub-par. These just are the opportunities and perils of a collective endeavour. 

This is even more so as even in cases in which the club has had no hand in doping being used, it still could have been more than an innocent bystander as such cheating went on in amongst itself. They would have had the option of adopting proactive measures to prevent it. We will see later why this is relevant.

Our arguments so far should show that clubs are appropriate targets for punishment when their players use PEDs. And in fact there is already some kind of precedent for such punishment, in clubs like Real Madrid being thrown out of a competition (the Copa del Rey) for fielding an ineligible (suspended) player. Both the impact of the illegitimate performance and the institutional involvement in both cases are similar enough to warrant the analogy.

Having come so far, we still have to figure out penalties that are proportionate and produce the right effects in practice. We will come back to the proportionality requirement later, and first think about what the desiderata for effectiveness should be.

What do we want to achieve with our anti-doping regulation? Are we trying to formulate rules that achieve maximal fairness in every single case? Or are we concerned with designing the rules in such a way as to bring about the best state of affairs - in our case, minimising the amount of doping going on - even if that would bring with it some less than perfectly just particular decisions. 

In a fit of slightly odd rule consequentialism, I am inclined to accept occasionally unjust statutes if they make it more likely that fewer players and teams will cheat. With regard to doping, this means creating incentives for all not to instigate, tolerate or orchestrate doping. The rule that I want to suggest is meant to do that.

So, what is it? Whenever a player is caught doping, all games he has played in within 3 weeks on either side of the date of the failed test will be counted as 3-0 defeats for the club.
Depending on the timing of the test and the identity of the player, such a penalty would have a significant impact on a club - in fact, it could wreck a season or even threaten the club's commercial existence.

Such a harsh sanction for the collective may seem excessive for an individual transgression. But as I have mentioned already, clubs are not resigned to a role as naive bystanders as cheating like this occurs. Rather, they can take action to prevent it, but will likely need strong reasons to do so. Powerful negative incentives, together with the creation of a credible doping control system, would generate such reasons and force clubs to ensure that their players are not doped when stepping on the field. This would involve first not to incentivise doping themselves, but likely also (repurposing) internal drug tests.

Still, this proposal entails the need to deal with some challenges. First, it would introduce or expand the possibility of results changing after the fact, but while competitions are still going on. This is particularly relevant in cup formats, where pairings in subsequent rounds may change by altering previous results. Obviously, if the round after the one that was affected by doping hasn't been played yet, it could simply go ahead with the team suffering a doping-afflicted defeat stepping back in. But if there were multiple rounds played under the influence, and more than one team has suffered unfair defeats, replaying the competition as it should have been may not be feasible. In such cases, the chemically enhanced team has to be taken out of the competition without replacement. This would not be a perfect solution, as it would not restore a tournament unaffected by doping, but it would at least minimise its influence.

Now, having made such a specific proposal, we have to engage somewhat with the empirical facts to show it's validity_________________


Choosing the date of the test as the anchor-point of the penalty is necessary to solve the delineation problem that doping presents. In contrast to other offenses (like playing a suspended player), doping doesn't necessarily 
occur in-game. Hence, we need to tie out-of-competition doping to meaningful fixtures to make the penalty effective. The suggestion here is probably not perfect, but a close-enough approximation. For positive results in the summer off-season, longer or moved suspensions may be considered.

The suggestion I have made here may not be perfect, but it is necessary. Under the current regulations, the most likely form of doping in football - apparent regrettable individual misconduct - is effectively free for the clubs benefiting from it, as post-hoc individual suspensions are even less of a deterrent here than they are in individual sports.

