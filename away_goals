<!DOCTYPE html>

<html>

<head>
 <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
 <meta content="Taktik, Fußball, Philosophie" name="keywords">
 <title> Pressschlag -- Das Forum für kontroverse Debatte</title>
 <!--<link rel="icon" href="../graphics/favicon.ico" type="image/icon">-->
 <link rel="icon" href="./favicon.ico" type="image/icon">
 <link href="./styles/pressschlag.css" type="text/css" rel="stylesheet">
</head>

<body>

<header>
 <nav>
   <ul>
	<li id="selected"><a href="./main">Home</a></li>
<!--	<li id="nonselected"><<a href="Background.html">Background</a>/li>
-->
<!--<li id="nonselected"><a href="./heiserneketten">Heiserne Ketten</a></li>-->
	<li id="nonselected"><a href="./about">About</a></li>
  </ul>
 </nav>
</header>

<section id="content">
	<div class="content">
<h1>Pressschlag</h1>

<h5>Das Forum für kontroverse Debatte</h5>

    <h3>Should away goals count for more?</h3>


     <h4 class="pro"> The Ayes</h4>  

     <p class="pro">
     That away goals are used as a differentiator when two-legged ties are level is met with 
<a href="http://bleacherreport.com/articles/2634844-its-time-for-football-to-kill-off-illogical-and-arbitrary-away-goals-rule" target="_blank"> weariness time and</a>
<a href="http://www.zeit.de/sport/2016-05/fc-bayern-atletico-auswaertstore/komplettansicht" target="_blank"> again,</a>
particularly if the team perceived to be superior falls victim to it.
The claim that away goals count double is just the beginning of what is wrong with this argument.
     </p>

<p class="pro">
To avoid some of the less productive paths that discussion might go, we don't want to argue here that the rule inspires more attacking play on behalf of either team. Rather, any closer inspection shows that there can be no other fair way to settle a tie - unless we are willing to consider replays, or, better yet, throwing dice.
</p>

<p class="pro">
Since the inception of the sort of competitions that employ the away goals rule much has changed in terms of the difference between playing at and away from home, with arenas across Europe becoming more and more similar (almost to a fault), and travel being much less cumbersome. However, there still is such a thing as 
<a href="http://www.economist.com/blogs/gametheory/2014/06/home-advantage-football" target="_blank"> home advantage</a>
- as becomes evident to anyone surveying home and away classements of elite leagues. 
Without adding weight to goals scored in the opposition's ground, there would be far more ties left to be decided after 180 minutes are played, as any draw would present the issue, and not only exactly reversed results. Unwilling to take recourse to the less-than-satisfying methods mentioned already, there is nothing left than to keep playing football in some way or other.
Because being at home does provide an advantage - on behalf of slow or slick surfaces, wide or narrow pitches, impressionable referees or 
<a href="https://www.youtube.com/watch?v=b50S9-yisJw" target="_blank"> frenetic ultras</a> - it would be unfair to give the home team an opportunity to make use of their advantage
The away goal rule offers a way
<a href="http://thepowerofgoals.blogspot.de/2013/03/the-away-goals-rule-in-extra-time-is.html" target="_blank"> to cancel</a>
<a href="https://www.researchgate.net/publication/279211418_What_is_a_good_result_in_the_first_leg_of_a_two-legged_football_match" target="_blank"> out this advantage.</a>
</p>

<p>
That extra time is what makes the away goal rule necessary suggests to some to only let it apply to <em> goals scored in it.</em>
However, that would be insufficient to ensure fair competition, as playing extra time at home gives teams the chance to win outright. To deal with that advantage, an extra-time-only-away-goals rule would have to in fact count away goals double. Obviously, the rule as is makes for a better solution than that.
</p>


     <h4 class="kontra"> Counter-point</h4>  
<p class="kontra">
To evaluate the merits of the rule, we first have to consider what it does: it turns game states that are tied into ones in which someone is winning.
</p>

<p class="kontra">
This description in itself suggests that the burden of argument rest with the proponents of the rule<sup><a href="#fn1" id="ref1">1</a></sup> - after all it is counterintuitive that it is possible to lose a goal by 2 goals to 2.  So, what may speak in favour of marking up away goals, and does it really?
</p>

<p class="kontra">
The main argument for the rule, historically as well as in popular discourse now, is that it gives road teams an incentive to attack. Yet, for the most part, any reason for one team to attack is a reason to defend for the other side. Gladly, we are not left to mere guessing in considering whether there is an added incentive to attack overall, but can look at statistical evidence instead.
</p>

<p class="kontra">
In order to do that, we simply look at the amount of shots taken by teams that are trailing in a match. It 
<a href="http://statsbomb.com/2013/12/score-effects/" target="_blank"> has been shown </a>
that when losing (by not a lot) teams do increase their shot volume.
But these statistics only show that the team that has gone behind takes a larger share of the shots in the game, not necessarily that there is more attacking football over all.
</p>

<p class="kontra">
But even if that were the case, it would only force a decision upon us to either decide games on the sporting basis of who scored more goals, or to prioritise the entertainment value of spectacular football.
That decision points to issues around which many opposing trends in (professional) football revolve.
</p>

<p class="kontra">
Though before we can get to an answer to that fundamental question, we do need to deal with an objection to our characterisation of the away-goals issue that would render it moot for our present purposes, clearing the mechanism of our charge of being an event-rule.
Ties with equal numbers of goals but differences in away goals are not really sporting ties, and there is a irreducible difference between them. That difference of course is only relevant to who should win the contest if it is more difficult to score away from home and doing so should be rewarded.
</p>

<p class="kontra">
To deny that significance, easier travel and otherwise diminished differences between home and away games are often called upon. But, there is still home advantage.But that fact is neither here nor there with regards to the rule discussed here, as if anything achieving good results away from is what is difficult. That of course is rewarded by helping to just win a tie - the away goal rule only has influence when both teams did equally well away from home.
And with that, we are back at the earlier point.
</p>

<p class="kontra">
My position with regards to it must be stated here without much argument: an essential part of the entertainment of sports is to see competitors try to outperform their opponents in developing and executing ideas to win. 
This is the entertaining element that is diminished when teams that are equal in their achievement are separated by an obsolete rule.
</p>

<p class="kontra">
<sup id="fn1" >1</sup>That is only inasmuch, as it is not always the case for any rule.</sup>
</p>

    <script id='fb8uywg'>(function(i){var f,s=document.getElementById(i);f=document.createElement('iframe');f.src='//button.flattr.com/view/?fid=0d5n9d&url='+encodeURIComponent(document.URL);f.title='Flattr';f.height=75;f.width=75;f.style.borderWidth=0;s.parentNode.insertBefore(f,s);})('fb8uywg');</script>


<h4>Copyleft-Notice</h4>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/au/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/au/88x31.png" /></a><br />These web pages are licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 License</a>.
	</div>
</body>

</html>
