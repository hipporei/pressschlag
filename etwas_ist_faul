<!DOCTYPE html>

<html>

<head>
 <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
 <meta content="Taktik, Fußball, Philosophie" name="keywords">
 <title> Pressschlag -- Das Forum für kontroverse Debatte</title>
 <!--<link rel="icon" href="../graphics/favicon.ico" type="image/icon">-->
 <link rel="icon" href="./favicon.ico" type="image/icon">
 <link href="./styles/pressschlag.css" type="text/css" rel="stylesheet">
</head>

<body>

<header>
 <nav>
   <ul>
	<li id="selected"><a href="./main">Home</a></li>
<!--	<li id="nonselected"><<a href="Background.html">Background</a>/li>
-->
<!--<li id="nonselected"><a href="./heiserneketten">Heiserne Ketten</a></li>-->
	<li id="nonselected"><a href="./about">About</a></li>
  </ul>
 </nav>
</header>

<section id="content">
	<div class="content">
<h1>Pressschlag</h1>

<h5>Das Forum für kontroverse Debatte</h5>

    <h3>Sollten wir Fouls hinnehmen?</h3>

    Also available to read 
    <a href="https://pressschlag.net/pressschlag/fouled_out" target="_blank" title="Against modern Fouls"> in English.</a>



     <h4 class="pro"> Corinthians</h4>  

     <p class="pro">
     Die letzten Minuten des Madrider Derby im Champions League Finale. Real greift mit zu schwacher Absicherung an, Atletico gewinnt den Ball, Yannick Carrasco umdribbelt drei weitere Weiße, eine nur mit Pepe besetzte Hälfte eröffnet sich vor ihm.
     </p>

     <p class="pro">
     Doch bevor Carrasco, Torres und co dort das Spiel entscheiden können, wird der Belgier von Sergio Ramos gefoult, mit einer Grätsche, die von schräg hinten in ihrem Bewegungsablauf ausschließlich darauf aus ist, und den Ball so sehr ignoriert wie Karl Jaspers beim 
     <a href="https://youtu.be/B6nI1v7mwwA?t=1m13s" target="_blank" title=""> Anstoß</a>. Er sieht dafür Gelb - und seine Mannschaft das Spiel gewinnen.
     </p>

     <p class="pro">
     Fouls sind oft Augenblick in Fußballspielen, an denen sich die Gemüter entzünden. Ein <em> Zeichen setzendes</em> Tackling des Gegenspielers mit dem meisten Flair wünschen sich (immer noch) viele, um die eigene Mannschaften zusammen zu schweißen. Einen eigenen Konter zynisch beendet zu sehen schafft einen Moment der Verbitterung. Ein überhartes Einsteigen einer Gastmannschaft ruft Anfeindungen der Heimfans hervor. Und, vor allem, all diese Reaktionen sind umso stärker, wenn der Schiedsrichter nicht im eigenen Sinn darüber befindet.
     </p>

     <p class="pro">
     Und trotzdem denken die meisten Beobachter, dass all das zum Spiel gehört, und, insbesondere wenn geahndet, weder Handlungs- noch Klärungsbedarf zurücklässt. Wenige scheinen Fouls als inhärent problematisch zu empfinden.
     </p>

     <p class="pro">
     Diese Haltung ist falsch.
Wenn Menschen Fußball spielen tun sie das, zumindest auf einer unmittelbaren Ebene, um zu sehen, wem das Spiel besser gelingt, oder, weniger kompetitiv gedacht, um in den Bewegungen des Spiels aufzugehen. Fouls passen nicht in dieses Schema.
     </p>

     <p class="pro">
Jedenfalls nicht, wenn wir außerhalb des Spiels liegende Gründe, gewinnen zu wollen, für den Moment außen vor lassen und uns auf Fouls beschränken, die nicht fehlgeschlagene Versuche sind, regelgerecht zu spielen. Mit Absicht und bewusst etwas zu tun, das die Regeln des Spiels verletzt, negiert den Sinn des spielerischen Unternehmens. 
     </p>

     <p class="pro">
Zur Illustration sowohl der Gründe dafür, die gängige Einstellung Fouls gegenüber zu überdenken, als auch der Attitüde, die sie ersetzen sollte, dienen zwei Beispiele. Zum einen stelle man sich vor, wie Schachspieler, -beobachter  oder Turnierleitungen auf den Versuch eines Kontestanten reagieren würden, die eigene Stellung zu verbessern, indem er einen Läufer auf einer horizontalen Linie zieht.
     </p>

     <p class="pro">
Man male sich außerdem aus, wie ein Heimpublikum darauf reagieren würde, wenn ein Spieler der eigenen Mannschaft einen Ball, für alle ersichtlich intendiert und kontrolliert, statt zu einem Mitspieler ins Aus passt - und so statt gegen die Regeln des erlaubten gegen die des richtigen Spiels verstieße. 
     </p>

     <p class="pro">
Diese Fälle sind offensichtlich in einigen Elementen absichtlichen Fouls unähnlich. Schach ist ein nicht -körperliches Spiel mit einem extrem eindeutig definierten Möglichkeitsraum. Spielerisches Fehlverhalten wie beschrieben verringert die eigenen Siegchancen ohne Gegenwert. Und keins von beiden passiert jemals. 
     </p>

     <p>
Doch im wesentlichen sind die Situationen sich ähnlich: Wie geht man mit Handeln um, dass in der gemeinsamen Aktivität möglich ist, aber die Prinzipien verletzt, durch die jener geteilte Handlungsraum konstituiert wird.
     </p>

     <p>
     Eine allgemein akzeptierte und adaptierte Haltung, die solches Handeln geradezu unmöglich macht (Schach) oder, sollte es vorkommen, als Ausfall, der nicht Normalität werden kann, behandelt (falsches Spielen), scheint den Gründen, aus denen man überhaupt spielt, mehr zu entsprechen als der status quo.
     </p>

     <p class="pro">
     Das bringt uns zu der Frage, wie diese abstrakten Überlegungen mit der Realität in Kontakt kommen könnten. Eine Idee wäre, den Geist des Spiels und den der Regeln zu synthetisieren: die Strafen für Fouls, die als zynisch, dem Spiel fremd angesehen werden, könnten drastisch verschärft werden - etwa mit roten Karten statt Verwarnungen. Gegen diesen Vorschlag wird vorgebracht, dass damit die Schiedsrichter_innen über die Absichten der Spieler zu entscheiden hätten, womit ihre Rolle überfrachtet wäre.
     </p>

     <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">These kind of tactical fouls should be punished more. A clear red for me, only purpose is to stop the opponent. <a href="https://t.co/ByZEDspA6K">pic.twitter.com/ByZEDspA6K</a></p>&mdash; István Beregi (@szteveo) <a href="https://twitter.com/szteveo/status/741670553648521216">June 11, 2016</a></blockquote>
     <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

 
     <p class="pro">
     Diese Sorge ist berechtigt, und die Schwierigkeiten in der Auslegung und Anwendung einer solchen Regel zeigen sich etwa bei der Bewertung des Handspiels.
Aber ich denke, dass dieses Problem für den Vorschlag nicht fatal ist: In die Bewertung (der Schwere) vieler Fouls schon jetzt fließt ein, nach welchen Prinzipien sich das Handeln eines Spielers gerichtet hat, etwa im Kriterium des In-Kauf-nehmens einer Verletzung des Gegners, das mit Platzverweis bestraft wird. Zu diesen Entscheidungen bedarf es keines Einblicks in die inneren Beweggründe des anderen - die Gründe, die irgendjemanden dazu bewegen könnten, zu tun was 
<em>de facto</em> geschehen ist, reichen als Grundlage des Urteils aus. Wie in anderen Teilen des Regelwerks wird es auch hier ausreichen, 'deliberate actions' zu erkennen, und kein direkter Zugriff auf innere Motivationen und Intentionen notwendig sein. Dass die Entscheidungen darüber nicht immer eindeutig sein werden und Schiedsrichtern viel abverlangen, unterscheidet sie nicht von irgendetwas anderem im Spiel. 
     </p>

     <p class="pro">
     Auch ohne eine solche Änderung einzuführen bieten sich Reformen an, mit denen den Überlegungen hier Rechnung getragen werden kann. Eine davon ist ein Paradigmenwechsel in der Bewertung taktischer Fouls. Diese werden bisher umso härter bestraft, je besser und konkreter die Torchance ist, die sie verhindern. Insofern also darauf geachtet wird, die Vor- und Nachteile für jedes Team ohne das Foul durch Strafen zu rekonstruieren, und so den Ausgang der Situation anzugleichen, ist dieses Modell konsequentialistisch.
     </p>

     <p class="pro">
     Stattdessen wolllen wir den Anspruch, das Spiel fair auszuüben,stärken. Daraus folgt eine Umkehrung der bisherigen Praxis: je geringer die Chance sein muss, für die ich bereit bin, den Bereich fairen Spiels zu verlassen, desto geringer schätze ich dieses Ideal offenbar - und verdiene härtere Sanktionen. Die Bewertung von regelwidrigem Spiel würde sich also danach richten, wie sehr ich darauf festgelegt bin, den Regeln zu folgen, und wie schnell ich mich bereit zeige, sie zu missachten. In anderen Worten, es richtete sich nach 
     <a href="http://plato.stanford.edu/entries/ethics-deontological/" target="_blank" title="Stanford Encyclopedia Erläuterung"> deontologischen Prinzipien</a>.
     </p>

     <p class="pro">
     Auch an dieser Änderung kann ausgesetzt werden, dass sie wie der vorherige Vorschlag zu einer enormen Inflation der Strafen führen würde. Dass diesen Regeln nach jedes intensionale Foul oder jedes Konter-verhindernde Halten Rot bedeutete, scheint eine <em>reductio ad absurdum</em> des Vorschlags zu liefern.
     </p>

     <p class="pro">
     In Antwort auf dieses Argument könnte man mit dem Schlachtruf <em>fiat justitia et pereat mundus</em> auf das Geschoss beißen. Oder eine Bagatellgrenze oder Vor- und Verwarnungssystem einführen, und außerdem auf eine Anpassung des Verhaltens der Spieler hoffen. 
     Damit, einen praktikablen Weg zum Ausschluss von Fouls zu finden, kommen wir jedoch zurück zu den grundsätzlichen Entscheidungen in dieser Frage, denn viele würden darin wohl einen Schritt sehen, der Fußball nicht attraktiver macht und insistieren, dass Fouls ebenso zum Fußball gehören wie als sie Mittel zur Unterbindung oder Verkürzung von Angriffen im Handball oder Basketball selbstverständlich sind.
     </p>

     <p class="pro">
     Damit wird deutlich, dass es letztlich um eine Werteentscheidung über eine Aufwertung des spielerischen Kerns des Fußballs geht. Diese Entscheidung soll hier nicht getroffen, sondern Nachdenken darüber angeregt werden.
     </p>

<figure> <img src="./corinthian.jpg" alt="Corinthians Arena" width="640" height="138">
 <figcaption> Das Stadion der 
	 <em>Corinthians</em>, die das Ideal im Namen tragen, nach dem Spieler in den frühen Tagen des Fußball angeblich Elfmeter mit Absicht verschossen, da der Gegner ja wohl unmöglich unsportlich genug gewesen sei, absichtlich zu foulen und so diese Strafe zu verdienen. 
	 <span style="font-size:10px"> <a href="https://www.flickr.com/photos/cbnsp/14289430737/" target="_blank">Bild</a> : © <a href="https://creativecommons.org/licenses/by/2.0/" target="_blank">CC-by 2.0</a>  <a href="https://www.flickr.com/photos/cbnsp/" target="_blank">Milton Jung</a> </span> 
 </figcaption>
</figure>

     <h4 class="kontra"> Kontrapunkt</h4>  

     <p class="kontra">
     <a href="http://fokus-fussball.de/category/collinaserben/" target="_blank" title="der Stimme der Schiedsrichterschaft"><em>Mit</em></a>
<a href="https://twitter.com/CollinasErben" target="_blank" title=""> Alex</a>
<a href="https://lizaswelt.net/" target="_blank" title=""> Feuerherdt</a>
     </p>

     <p class="kontra">
Fouls, insbesondere 'absichtlich' begangene, aus dem Fußball ausschließen zu wollen ist eine recht exotische Position. Als solcher obliegt ihr eine schwere Beweislast zu zeigen, dass ihre Umsetzung erstrebenswert, praktikabel und dem Spiel und den Spielern gegenüber gerecht wäre. Der Schiedsrichter (-ausbilder und -beobachter), Podcaster und Kolumnist Alex Feuerherdt ist in all diesen Belangen höchst skeptisch.
     </p>

<p class="kontra">
Ein solches Kriterium in die schiedsrichterliche Bewertung einzuführen verlange von den Unparteiischen über die ohnehin höchst anspruchsvolle Aufgabe der Wahrnehmung und unmittelbaren Bewertung des Spielgeschehens entlang der Frage, welcher Einsatz zulässig und welcher irregulär ist, hinaus auch noch zu "einer moralischen Instanz zu werden. Eine solche ist er nicht, und das ist auch gut so. Man würde sein Amt überfrachten, wenn man von ihm noch mehr verlangen würde." 
</p>

<p class="kontra">
Eine weitere Einteilung von Vergehen, um solche von "besonderer moralischer Verwerflichkeit" nach eigenen Maßstäben zu bestrafen, wäre dem hier diskutierten Ansatz zu Folge notwendig. Dies würde aber voraussetzen, dass Schiedsrichter Entscheidungen über die Absichten der Spieler treffen. Da diese jedoch nicht nach Außen sichtbar sind, ist das schlicht unmöglich, und wird genau aus diesem Grund auch an keiner Stelle des Regelwerks (zumindest in Bezug auf Foulspiel) verlangt. Gerade die vielen Tausend Schiedsrichter_innen auf Amateurfußballplätzen sollten nicht mit noch komplexeren Aufgaben als den jetzt vorgesehenen belastet werden, wenn eine möglichst korrekte Anwendung der Regeln angestrebt wird. </p>

<p class="kontra">
Gerade weil "Fußball ein Kontaktsport ist, wird es immer wieder Grenzfälle und eine große Grauzone geben, der dem Schiedsrichter einen gewissen Ermessensspielraum gibt." Wollte man Fouls in der vorgeschlagenen Weise stärker ächten, hieße das, um minimal konsistente Entscheidungen zu ermöglichen, solche Grauzonen zu vermeiden und rigide Maßstäbe anzulegen. Damit würde es für irgendein Textilvergehen irgendwo auf dem Platz drakonische Strafen geben - "das würde von sehr vielen Menschen als unangemessen empfunden werden," so Feuerherdt. Die gegenwärtige Auslegung der Regeln genieße dagegen weite Akzeptanz, es wäre mithin fahrlässig und fatal, sich von Ausnahmefällen (wie dem Foul von Sergio Ramos an Carrasco) zu Verschärfungen verleiten zu lassen.
</p>

<p class="kontra">
Im Gegenteil tendierten die Regelhüter sogar eher dazu, gewisse Sanktionen für Fouls, die Torgelegenheiten unterbinden, zu lockern. Feuerherdt findet daran auch nichts falsches, immerhin würde hier zwar Foul gespielt, dem Gegner aber oft kein nenneswerter physischer Schaden zugefügt. Teil der Qualität, die im Spiel verlangt wird, ist auch, mit gegnerischem Einsatz jenseits des Erlaubten umgehen zu können. Das Ideal der technischen Brillanz, die selbst Fouls unmöglich macht, illustriert, dass damit spielerisch hochwertiger Fußball durchaus nicht an seiner Entfaltung gehindert wird.
</p>


<p>
Dieser Text basiert auf einem Interview, das ich mit Alex geführt habe, indem er sich auch Status der 
<a href="https://pressschlag.net/pressschlag/was_der_fall_ist" target="_blank" title="Pro/Kontra Tatsachen"> Tatsachenentscheidun</a>
geäußert hat. Das Interview ist hier zu hören:
         <audio controls>
	 <source src="./Ps_alex_feuerherdt.opus" type="audio/ogg">
		 Dein Browser unterstützt das freie ogg/opus Format nicht. Du kannst das Interview 
<a href="./Ps_alex_feuerherdt.opus" target="_blank" title=""> herunterladen</a>
		 und anhören.
	 </audio>
</p>




    <script id='fb8uywg'>(function(i){var f,s=document.getElementById(i);f=document.createElement('iframe');f.src='//button.flattr.com/view/?fid=0d5n9d&url='+encodeURIComponent(document.URL);f.title='Flattr';f.height=75;f.width=75;f.style.borderWidth=0;s.parentNode.insertBefore(f,s);})('fb8uywg');</script>

<h4>Copyleft-Notice</h4>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/au/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/au/88x31.png" /></a><br />These web pages are licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 License</a>.
	</div>
</body>

</html>
