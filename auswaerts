<!DOCTYPE html>

<html>

<head>
 <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
 <meta content="Taktik, Fußball, Philosophie" name="keywords">
 <title> Pressschlag -- Das Forum für kontroverse Debatte</title>
 <!--<link rel="icon" href="../graphics/favicon.ico" type="image/icon">-->
 <link rel="icon" href="./favicon.ico" type="image/icon">
 <link href="./styles/pressschlag.css" type="text/css" rel="stylesheet">
</head>

<body>

<header>
 <nav>
   <ul>
	<li id="selected"><a href="./main">Home</a></li>
<!--	<li id="nonselected"><<a href="Background.html">Background</a>/li>
-->
<!--<li id="nonselected"><a href="./heiserneketten">Heiserne Ketten</a></li>-->
	<li id="nonselected"><a href="./about">About</a></li>
  </ul>
 </nav>
</header>

<section id="content">
	<div class="content">
<h1>Pressschlag</h1>

<h5>Das Forum für kontroverse Debatte</h5>

    <h3>Sollte die Auswärts-Tor Regel bestehen?</h3>

    Read 
    <a href="http://pressschlag.net/away_goals" target="_blank"> in English</a>

     <h4 class="pro"> The Ayes</h4>  

     <p class="pro">
Dass erzielte Auswärtstore bei Gleichstand als Entscheidungsmechanismus benutzt werden, führt immer wieder zu 
<a href="http://bleacherreport.com/articles/2634844-its-time-for-football-to-kill-off-illogical-and-arbitrary-away-goals-rule" target="_blank"> Beschwerden,</a>
vor allem, wenn eine 
<a href="http://www.zeit.de/sport/2016-05/fc-bayern-atletico-auswaertstore/komplettansicht" target="_blank"> als besser wahrgenommene Mannschaft daran scheitert,</a>
dass 'Auswärtstore' doppelt zählten. Das sie das nicht tun ist nicht alles, was an diesem Argument falsch ist.
     </p>

<p class="pro">
Um einige der Aporien der Diskussion über diese Regel zu vermeiden, soll hier nicht behauptet werden, sie fördere Angriffsfußball der einen oder anderen Seite. Vielmehr wird bei genauer Betrachtung deutlich, dass es ohne sie keine Weise gibt, Pokalbegegnungen über Heim- und Auswärtsspiele fair zu entscheiden.
</p>

<p class="pro">
Auch wenn sich seit der Einführung derartiger Wettbewerbe die Verhältnisse in den verschiedenen Arenen angegeglichen haben (almost to a fault), so gibt es doch noch einen 
<a href="http://www.economist.com/blogs/gametheory/2014/06/home-advantage-football" target="_blank"> Heimvorteil</a>
- das wird schon aus der Betrachtung von Heim- und Auswärtstabellen verschiedenster Ligen deutlich.
Ohne die den Auswärtstor-Mechanismus gäbe es sehr viel öfter nach 180 Minuten einen Gleichstand zu entscheiden - nach allen Unentschieden, statt nur nach solchen mit gespiegelten Ergebnissen. Will man nun noch am selben Tag eine Entscheidung findet bleibt - außer dem Losentscheid - nichts, als weiter in irgendeiner Art Fußball zu spielen. Weil es einen Heimvorteil gibt - dank schnellem oder langsamen Rasen, weiten oder schmalen Spielfeldern, beeinflussbaren Schiedsrichtern oder 
<a href="https://www.youtube.com/watch?v=b50S9-yisJw" target="_blank"> enthusiastischen Fans</a> - wäre es nun unfair, der Heimmannschaft Gelegenheit zu geben, diesen Vorteil zu nutzen. Die Auswärtstorregel 
<a href="http://thepowerofgoals.blogspot.de/2013/03/the-away-goals-rule-in-extra-time-is.html" target="_blank"> wirkt dieser unfairen Konstellation</a>
<a href="https://www.researchgate.net/publication/279211418_What_is_a_good_result_in_the_first_leg_of_a_two-legged_football_match" target="_blank"> entgegen.</a>
</p>

<p class="pro">
Dass es die Verlängerung ist, die eine Auswärtstorregel notwendig macht, hat manche dazu gebracht, eine solche nur <em>für</em> die Verlängerung zu fordern. Das verkennt jedoch, dass der Vorteil für die Heimmannschaft im Rückspiel darin besteht, Gelegenheit zu haben, das Spiel nach Toren zu gewinnen. Auswärtstore müssten hier also tatsächlich doppelt zählen um den Nachteil auszugleichen. Das ist offensichtlich noch sehr viel problematischer als die Auswärtstorregel in ihrer bisherigen Form.
</p>


     <h4 class="kontra"> Kontra-Punkt</h4>  
<p class="kontra">
Um zu bewerten, ob die Auswärtstorregel beibehalten werden sollte, müssen wir uns zuerst anschauen, was sie tatsächlich tut:
Die Regel für Auseinandersetzungen über Hin- und Rückspiel, dass bei Unentschieden nach 180 Minuten die Mannschaft gewinnt, die mehr Tore auswärts erzielt hat. Das bedeutet, dass in mehr Spielständen eine Mannschaft gewinnt.
</p>

<p class="kontra">
Schon diese Beschreibung legt nahe, dass die Beweislast für die Sinnhaftigkeit der Regel bei ihren Befürwortern liegt<sup><a href="#fn1" id="ref1">1</a></sup> - schließlich ist es kontraintuitiv, dass man ein Fußballspiel 2-2 verlieren kann. Was könnte also für die höhere Gewichtung von auswärts geschossenen Toren sprechen, und tut es das tatsächlich?
</p>

<p class="kontra">
Das historische und zeitgenössische Hauptargument für die Auswärtstorregel besagt, dass sie die Gastmannschaft zu offensiverem Fußball bewegt. Nun ist aber in der Regel jeder Grund für eine Mannschaft, anzugreifen, zugleich einer für die andere, zu verteidigen. Zum Glück sind wir an dieser Stelle nicht auf reine Spekulation angewiesen, sondern können uns an statistischen Daten orientieren um festzustellen, ob dabei insgesamt mehr offensiver Fußball entsteht.
</p>

<p class="kontra">
Dazu schauen wir uns schlicht an, ob während eine Mannschaft in einem Spiel zurückliegt mehr Schüsse abgegeben werden als während es unentschieden steht. Zwar schießen nachweislich (knapp) zurückliegende Mannschaften 
<a href="http://statsbomb.com/2013/12/score-effects/" title="scoring effects" target="_blank"> im Versuch, auszugleichen, öfter.</a>
Doch diese Statistik zeigt nur, dass die zurückliegende Mannschaft einen höheren Anteil der Torschüsse in einem Spiel abgibt. Dass insgesamt offensiver gespielt wird, wenn eine Mannschaft führt, ist damit noch nicht gezeigt.
</p>

<p class="kontra">
Aber selbst wenn das der Fall wäre, hieße das nur, dass man mit der Entscheidung für oder gegen die Auswärtstorregel vor der Entscheidung steht, sportlich faire Bewertung der erbrachten Leistung, oder höheren Unterhaltungswert und spektakulärere Events zu fördern. Offensichtlich ist das eine eigene große Frage, die der Dreh- und Angelpunkt vieler gegenläufiger Tendenzen im (professionellen) Fußball ist.
</p>

<p class="kontra">
Aber bevor wir auf diese Frage antworten, müssen wir uns mit einem möglichen Argument beschäftigen, dass die Auswärtstor-Gewichtung vor dem Vorwurf, eine Event-Regel zu sein, bewahren möchte: Begegnungen, in denen eine Mannschaft einen solchen Vorteil hat, sind sportlich nicht unentschieden, schließlich gibt es einen echten Unterschied zwischen beiden Teams. Der ist freilich in der sportlichen Bewertung der Leistungen nur relevant, wenn es schwieriger ist, in fremden Stadien Tore zu schießen als im eigenen, und dies deshalb belohnt werden sollte. 
</p>

<p class="kontra">
An dieser Stelle wird oft eingewandt, dass der Unterschied zwischen Heim- und Auswärtsspielen mit standartisierten Bedingungen und leichterem Reisen über die letzten Jahrzehnte immer geringer geworden ist. Trotzdem besteht ein Heimvorteil noch immer. Für das Argument gegen die Priorisierung von Auswärtstoren ist das aber kein Problem: hier werden schließlich nicht bessere Ergebnisse in Auswärtsspielen, sondern nur mehr erzielte Tore belohnt (denn die erreichten Ergebnisse sind natürlich symmetrisch). Und damit sind wir wieder zurück bei der Gewichtung des Spektakels.
</p>

<p class="kontra">
Meine Antwort auf die oben gestellte Frage, muss an dieser Stelle ohne echte Begründung auskommen: Ein nicht unwesentlicher Teil der Unterhaltung, die Fußball wie anderer Sport bietet, besteht darin zu sehen, wie Kontrahenten versuchen, bessere Ideen besser umzusetzen und zu gewinnen. Dieser Unterhaltungsfaktor wird gedämpft, wenn Gewinner nicht fair ermittelt werden. Deshalb ist die Auswärtstorregel falsch.
</p>

<p class="kontra">
<sup id="fn1" >1</sup>Das ist hier insofern gemeint, als es nicht ohnehin für alle Regeln der Fall ist.<a href="./#ref1" title="Zurück zum Punkt.">↩</a></sup>
</p>

    <script id='fb8uywg'>(function(i){var f,s=document.getElementById(i);f=document.createElement('iframe');f.src='//button.flattr.com/view/?fid=0d5n9d&url='+encodeURIComponent(document.URL);f.title='Flattr';f.height=75;f.width=75;f.style.borderWidth=0;s.parentNode.insertBefore(f,s);})('fb8uywg');</script>


<h4>Copyleft-Notice</h4>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/au/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/au/88x31.png" /></a><br />These web pages are licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 License</a>.
	</div>
</body>

</html>
